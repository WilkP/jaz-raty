package servlet;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOError;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerFontProvider;
import com.itextpdf.tool.xml.XMLWorkerHelper;

@WebServlet("/calc")
public class CreditServlet extends HttpServlet {
	
	public static final String PDFFILE = "src/main/resources/plan_splaty.pdf";
	public static final String CSSFILE = "src/main/webapp/styles.css";
	public static final String FONT = "src/main/resources/lato.ttf";
	
	public void createPDF(String sciezkaDoPDF, String sciezkaDoCSS, String trescDokumentu) throws DocumentException, IOException {
		InputStream cssFile = new FileInputStream(sciezkaDoCSS);		
		
		Document doc = new Document(PageSize.A4);
		
		PdfWriter pdfWriter = PdfWriter.getInstance(doc, new FileOutputStream(sciezkaDoPDF));
		pdfWriter.setInitialLeading(12.5f);
		
		doc.open();
		doc.addCreator("Kalkulator Rat");
		doc.addSubject("Plan splaty kredytu");
		doc.addCreationDate();
	    doc.addTitle("Plan splaty kredytu");
	    
	    ByteArrayInputStream trescHtml 
	    		= new ByteArrayInputStream(trescDokumentu.getBytes("UTF-8"));
	    
	    XMLWorkerFontProvider fontImp
	    		= new XMLWorkerFontProvider(XMLWorkerFontProvider.DONTLOOKFORFONTS);
        fontImp.register(FONT);
        
	    XMLWorkerHelper worker = XMLWorkerHelper.getInstance();
	    worker.parseXHtml(
	    		pdfWriter,
	    		doc,
	    		trescHtml,
	    		cssFile,
	    		Charset.forName("UTF-8"),
	    		fontImp
	    );
	    doc.close();
	    
	    System.out.println("Dokument zosta³ utworzony!");
	}
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException{
		
		double creditAmount = Double.parseDouble(request.getParameter("creditamount"));
		double installmentNumber = Double.parseDouble(request.getParameter("installmentnumber"));
		double percentage = Double.parseDouble(request.getParameter("percentage"));
		double constantPayment = Double.parseDouble(request.getParameter("constantpayment"));
		String installmenttype = request.getParameter("installmenttype");
		
		double	saldo = 0,
				czescOdsetkowaRaty = 0,
				wysokoscRaty = 0,
				czescKapitalowaRaty = 0,
				oplataStala = 0,
				suma_rat = 0,
				suma_odsetek = 0,
				suma_kapitalowa = 0;
		
		String 	trescDokumentu ="<html lang='pl'>"
				+ "<head>"
				+ "<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>"
				+ "<title>Kalkulator rat kredytu</title>"
				+ "<link href='./styles.css' rel='stylesheet'/>"
				+ "<link href='https://fonts.googleapis.com/css?family=Lato:400,300,300italic,700,400italic&subset=latin,latin-ext' rel='stylesheet' type='text/css'/>"
				+ "</head>"
				+ "<body style='font-family: Lato, sans-serif; text-align: center;'>"
				+ "<h1>Harmonogram sp³at</h1>"
				+ "<p>Kwota kredytu: " + creditAmount + " z³</p>"
				+ "<p>Iloœæ rat: " + installmentNumber + "</p>"
				+ "<p>Rodzaj rat: " + installmenttype + "</p>"
				+ "<p>Oprocentowanie kredytu: " + percentage * 100 + " %</p>"
				+ "<p>Prowizja: " + constantPayment + " %</p>"
				+ "<table>"
				+ "<tr>"
				+ "<td>Nr raty</td>"
				+ "<td>Czêœæ kapita³owa raty</td>"
				+ "<td>Kwota odsetek</td>"
				+ "<td>Ca³kowita kwota raty</td>"
				+ "<td>Op³aty sta³e</td>"
				+ "<td>Kwota kapita³u</td>"
				+ "</tr>",
			wierszTabeli = "",
			stopkaTabeli = "";
		
		if("decreasing".equals(installmenttype)){
			czescKapitalowaRaty = (creditAmount * (1 + constantPayment)) / installmentNumber; // wyliczenie sta³ej czêœci kapita³owej
			oplataStala = (creditAmount * constantPayment) / installmentNumber; // wyliczenie sta³ej op³aty za kredyt
			
			
			for (int i = 1; i < installmentNumber + 1; i++) {
				
				saldo = (creditAmount * (1 + constantPayment)) * (1 - (i / installmentNumber)); // wyliczenie salda po wp³acie raty
				czescOdsetkowaRaty = czescKapitalowaRaty * percentage * (installmentNumber - i + 1); // wyliczenie czesci odsetkowej raty
				wysokoscRaty = czescKapitalowaRaty * (1 + (percentage * (installmentNumber - i + 1))); // wyliczenie ca³kowitej kwoty raty
				
				
				suma_rat += wysokoscRaty;
				suma_odsetek += czescOdsetkowaRaty;
				suma_kapitalowa += czescKapitalowaRaty;				
				
				
				wierszTabeli = "<tr>"
						+ "<td>" + i + "</td>"
						+ "<td>" + Math.round(czescKapitalowaRaty * 100d) / 100d + " z³</td>"
						+ "<td>" + Math.round(czescOdsetkowaRaty * 100d) / 100d + " z³</td>"
						+ "<td>" + Math.round(wysokoscRaty * 100d) / 100d + " z³</td>"
						+ "<td>" + Math.round(oplataStala * 100d) / 100d + " z³</td>"
						+ "<td>" + Math.round(saldo * 100d) / 100d  + " z³</td>"
						+ "</tr>";
				
				trescDokumentu += wierszTabeli;
			}
		
		}
		if("constant".equals(installmenttype)){
			double 	suma_oprocentowan = 0,
					pomocniczaKwotaKredytu = (creditAmount * (1 + constantPayment));
			
			for (int i = 1; i < installmentNumber + 1; i++) {
				suma_oprocentowan += (1 / Math.pow((1 + percentage), i));
			}
			
			wysokoscRaty = pomocniczaKwotaKredytu / suma_oprocentowan;
						
			for (int i = 1; i < installmentNumber + 1; i++) {
				pomocniczaKwotaKredytu -= czescKapitalowaRaty; 
				czescOdsetkowaRaty = pomocniczaKwotaKredytu * percentage; // wyliczenie czêœci odsetkowej raty
				czescKapitalowaRaty = wysokoscRaty - czescOdsetkowaRaty; // wyliczenie czêœci kapita³owej raty
				saldo = (creditAmount * (1 + constantPayment)) - (i * wysokoscRaty); // wyliczenie salda po wp³acie raty
				
				suma_rat += wysokoscRaty;
				suma_odsetek += czescOdsetkowaRaty;
				suma_kapitalowa += czescKapitalowaRaty;
				
				wierszTabeli = "<tr>"
						+ "<td>" + i + "</td>"
						+ "<td>" + Math.round(czescKapitalowaRaty * 100d) / 100d + " zl</td>"
						+ "<td>" + Math.round(czescOdsetkowaRaty * 100d) / 100d + " zl</td>"
						+ "<td>" + Math.round(wysokoscRaty * 100d) / 100d + " zl</td>"
						+ "<td>" + Math.round(oplataStala * 100d) / 100d + " zl</td>"
						+ "<td>" + Math.round(saldo * 100d) / 100d  + " zl</td>"
						+ "</tr>";
				
				trescDokumentu += wierszTabeli;
			}
		}
		stopkaTabeli = "<tr>"
				+ "<td>SUMA</td>"
				+ "<td>" + Math.round(suma_kapitalowa * 100d) / 100d + " zl</td>"
				+ "<td>" + Math.round(suma_odsetek * 100d) / 100d + " zl</td>"
				+ "<td>" + Math.round(suma_rat * 100d) / 100d + " zl</td>"
				+ "<td>" + Math.round(oplataStala * 100d) / 100d + " zl</td>"
				+ "<td>" + Math.round(creditAmount * 100d) / 100d + " zl</td>"
				+ "</tr>"
				+ "</table>"
				+ "<p>Koszt kredytu: " + Math.round(suma_odsetek * 100d) / 100d + " zl</p>"
				+ "<p>Kwota do splaty: " + Math.round(suma_rat * 100d) / 100d + " zl</p>";
		
		if ("Oblicz".equals("submit")) {
			// wypisanie tresci dokumentu			
			response.getWriter().println(
					"<!DOCTYPE html>"
					+ trescDokumentu
					+ "<a href='/'>Powrót do formularza</a>"
					+ "</body></html>"
			);
		} else {
			try {
				String redirect = "<!DOCTYPE html>"
						+ "<html lang='pl'>"
						+ "<head>"
						+ "<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>"
						+ "<title>Kalkulator rat kredytu</title>"
						+ "<link href='./styles.css' rel='stylesheet'/>"
						+ "<link href='https://fonts.googleapis.com/css?family=Lato:400,300,300italic,700,400italic&subset=latin,latin-ext' rel='stylesheet' type='text/css'/>"
						+ "</head>"
						+ "<body style='font-family: Lato, sans-serif; text-align: center;'>"
						+ "<h1>Dokument wyeksportowany pomyœlnie!</h1>"
						+ "<a href='/'>Powrót do formularza</a>"
						+ "</body></html>";
				
				createPDF(PDFFILE, CSSFILE, trescDokumentu + "</body></html>");
				response.getWriter().println(redirect);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
		}

	}
}
